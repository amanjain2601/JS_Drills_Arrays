function reduce(elements, cb, startingValue = elements[0]) {
    if (reduce.arguments.length == 2) {
        for (let index = 1; index < elements.length; index++) {
          startingValue = cb(startingValue, elements[index]);
        }
    }

    else {
        for (let index = 0; index < elements.length; index++) {
            startingValue = cb(startingValue, elements[index]);
        }
    }

    return startingValue;
}

module.exports = reduce