let arr = [];
function map(elements, cb) {
    for (let index = 0; index < elements.length; index++) {
        let transformElement = cb(elements[index]);
        arr.push(transformElement);
    }

    return arr;

}

module.exports = map;