function filter(elements, cb) {
    let arr = [];

    for (let index = 0; index < elements.length; index++) {
        let answer = cb(elements[index]);
        if (answer == true)
            arr.push(elements[index]);
    }

    return arr;
}

module.exports = filter;