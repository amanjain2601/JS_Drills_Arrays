function find(elements, cb) {
    for (let index = 0; index < elements.length; index++) {
        let answer = cb(elements[index]);
        if (answer == true)
            return elements[index];
    }

    return undefined;
}

module.exports = find;