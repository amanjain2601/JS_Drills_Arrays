const items = [1, 2, 3, 4, 5, 5];
const reduce = require("../reduce.js");

function sum(startingValue, element) {
    return startingValue + element;
}

let ans = reduce(items, sum, 100);

console.log("Sum of Array is", ans);