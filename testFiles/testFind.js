const items = [1, 2, 3, 4, 5, 5];
const find = require("../find.js");

function evenNumber(element) {
    if (element % 2 == 0)
        return true;
}

let answer = find(items, evenNumber);

if (answer != undefined)
    console.log("Even number in array is", answer);
else
    console.log("Array only contains odd numbers in it");
